# -*- coding: utf-8 -*-
"""
unit_tests.test_profile

Module contains unittests for view in core app.

:copyright: (c) 2015 by Pavlo Zhmak.
:license: BSD.
"""

import unittest

from django.test import Client
from django.core.urlresolvers import reverse

from ...mainteacher.models.teachers import Teachers


class TestProfile(unittest.TestCase):
    """TestCase class for checking profile view."""

    def setUp(self):
        self.client = Client()

        user = Teachers.objects.get(id=1)

        self.session = self.client.session
        self.session['teacher_id'] = user.id
        self.session['teacher_role'] = user.role.role_name
        self.session.save()

    def tearDown(self):
        del self.client

    def test_profile_page(self):
        """Check profile."""
        response = self.client.get(reverse('profile'))
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
