# -*- coding: utf-8 -*-
"""
unit_tests.test_login

Module contains unittests for view in core app.

:copyright: (c) 2015 by Pavlo Zhmak.
:license: BSD.
"""

import unittest

from django.test import Client
from django.core.urlresolvers import reverse

from ...mainteacher.models.teachers import Teachers


class TestLogin(unittest.TestCase):
    """TestCase class for checking kogin page function."""

    def setUp(self):
        self.client = Client()

        self.session = self.client.session
        self.session['teacher_id'] = 1
        self.session.save()

    def tearDown(self):
        del self.client

    def test_sign_in(self):
        """Check sign in."""
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)

        # set test credentials from test_SMSDB
        data = {'inputUsername': 'zoshch', 'inputPassword': 'df5sFdf'}
        response = self.client.post(reverse('login'), data=data)
        self.assertEqual(response.status_code, 302)

    def test_logout(self):
        """Check logout."""
        response = self.client.get(reverse('logout'))
        self.assertEqual(len(self.client.session.items()), 0)
        self.assertEqual(response.status_code, 302)


if __name__ == '__main__':
    unittest.main()
