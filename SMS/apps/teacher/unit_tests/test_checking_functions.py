# -*- coding: utf-8 -*-
"""
unit_tests.test_checking_functions

Module contains unit test for check_editable function from utils.

:copyright: (c) 2015 by Sofia Kuzlo.
:license: BSD.
"""

import unittest
from datetime import date, timedelta

from ..utils.checking_functions import check_editable


class TestCheckEditable(unittest.TestCase):
    """This class is used to test check_editable function."""

    def setUp(self):
        self.field_today = date.today()
        self.field_yesterday = date.today() + timedelta(days=1)
        self.field_tomorrow = date.today() - timedelta(days=1)

    def tearDown(self):
        del self.field_today
        del self.field_yesterday
        del self.field_tomorrow

    def test_editable(self):
        """If argument has current date function returns True, else - False."""
        self.assertTrue(check_editable(self.field_today))
        self.assertFalse(check_editable(self.field_yesterday))
        self.assertFalse(check_editable(self.field_tomorrow))


if __name__ == '__main__':
    unittest.main()
