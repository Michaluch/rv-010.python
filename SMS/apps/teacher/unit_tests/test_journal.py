# -*- coding: utf-8 -*-
"""
unit_tests.test_journal

Module contains unit tests for journal view.

:copyright: (c) 2015 by Sofia Kuzlo.
:license: BSD.
"""

import unittest

from django.test import Client
from django.core.urlresolvers import reverse

from ..models.groups import Groups
from ..models.teacher_subject import TeacherSubjects
from ..models.teacher_subject_group import TeacherSubjectGroups


class TestJournal(unittest.TestCase):
    """This class is used to test journal view functions."""

    def setUp(self):
        self.client = Client()
        # create custom teacher_id for session
        self.session = self.client.session
        self.session['teacher_id'] = 4
        self.session.save()

        # add test group
        self.group, created = Groups.objects.get_or_create(name='4В')

        # add test teacher_subject
        self.teacher_subject, created = TeacherSubjects.objects.get_or_create(
            subject_id=1,
            teacher_id=4)

        # add test teacher_subject_group
        self.teacher_subject_group, created = (
            TeacherSubjectGroups.objects.get_or_create(
                group_id=self.group.id,
                teacher_subject_id=self.teacher_subject.id))

    def tearDown(self):
        del self.client
        del self.session
        self.group.delete()
        self.teacher_subject.delete()
        self.teacher_subject_group.delete()

    def test_marks_list(self):
        """Check view function marks_list."""
        url = reverse('class_journal',
            kwargs={'teacher_subject_group': self.teacher_subject_group.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_add_lesson(self):
        """Check view function add_lesson."""
        url = reverse('class_journal_add_lesson',
            kwargs={'current_teacher_group': self.teacher_subject_group.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
