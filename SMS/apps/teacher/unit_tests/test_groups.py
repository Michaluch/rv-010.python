# -*- coding: utf-8 -*-
"""
unit_tests.test_groups

Module contains unit tests for groups view.

:copyright: (c) 2015 by Sofia Kuzlo.
:license: BSD.
"""

import unittest

from django.test import Client
from django.core.urlresolvers import reverse


class TestGroups(unittest.TestCase):
    """This class is used to test groups view functions."""

    def setUp(self):
        self.client = Client()
        # create custom teacher_id for session
        self.session = self.client.session
        self.session['teacher_id'] = 4
        self.session.save()

    def tearDown(self):
        del self.client
        del self.session

    def test_subject_group_list(self):
        """Check view function subject_group_list."""
        url = reverse('subject_group_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
