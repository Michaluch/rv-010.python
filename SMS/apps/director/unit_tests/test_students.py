# -*- coding: utf-8 -*-
"""
unit_tests.test_students

Module contains unittests for view functions in director app.

:copyright: (c) 2015 by Zhmak Pavlo.
:license: BSD.
"""

import unittest

from django.test import Client
from django.core.urlresolvers import reverse

from ...teacher.models.groups import Groups


class TestStudents(unittest.TestCase):

    """TestCase class for checking students view function."""

    def setUp(self):
        self.client = Client()

        # create custom teacher_id for session
        self.session = self.client.session
        self.session['teacher_id'] = 1
        self.session.save()

        # add test group
        self.group, created = Groups.objects.get_or_create(id=1,
                                                           school_id=1,
                                                           name='1А',
                                                           teacher_id=1)

    def tearDown(self):

        self.group.delete()

    def test_student_list(self):
        """Check view function student_list."""
        url = reverse('student_list',
                      kwargs={'group_id': self.group.id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_student_add(self):
        """Check view function student_add."""
        url = reverse('student_add',
                      kwargs={'group_id': self.group.id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()
