# -*- coding: utf-8 -*-
"""
unit_tests.test_fire_school_director

Module contains unittest for fire_school_director function from utils
package in mainteacher application.

:copyright: (c) 2015 by Oleksii Omelchuk.
:license: BSD.
"""

import unittest

from ..models.schools import Schools
from ..models.teachers import Teachers
from ..utils.manage_teachers import fire_school_director


class TestsMainteacherSetDefaultRole(unittest.TestCase):
    """TestCase class for checking fire_school_director function."""

    def setUp(self):
        self.school = Schools(name='Test_school', address='some street')
        self.school.save()
        self.director = Teachers(name='Person for test', login='test',
                                 email='test@gmail.com', role_id=3,
                                 password='test_example',
                                 school_id=self.school.pk)
        self.director.save()
        self.school.director_id = self.director.pk
        self.school.save()

    def tearDown(self):
        self.director.delete()
        self.school.delete()
        del self.director
        del self.school

    def test_set_default_role(self):
        """Testing remove director in school."""
        fire_school_director(self.school.director_id)
        self.school = Schools.objects.get(id=self.school.pk)
        self.assertEqual(self.school.director_id, None)


if __name__ == '__main__':
    unittest.main()
