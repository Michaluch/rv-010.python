# -*- coding: utf-8 -*-
"""
unit_tests.test_school_validate_form

Module contains unittest for school_validate_form function from utils
package in mainteacher application.

:copyright: (c) 2015 by Oleksii Omelchuk.
:license: BSD.
"""

import unittest

from django.http import HttpRequest

from ..utils.validation_school import school_validate_form


class TestsMainteacherSchoolFormValidation(unittest.TestCase):
    """TestCase class for checking inputted data in mainteacher
    school form."""

    def setUp(self):
        self.request = HttpRequest
        self.request.POST = {
            'name': u'НВК №12',
            'address': u'вул. Грушевського, 81',
            'director': 3
        }
        self.data = {}
        self.errors = {}

    def tearDown(self):
        del self.request
        del self.data
        del self.errors

    def test_school_validation_of_correct_inputed_data(self):
        """Test correct validation of school form."""
        school_validate_form(self.request, self.errors, self.data)
        self.assertEqual(len(self.errors), 0)

    def test_school_validation_of_incorrect_inputed_name(self):
        """Test validation of school form
        when name was inputted incorrect.
        """
        self.request.POST['name'] = u'НВК 12'
        school_validate_form(self.request, self.errors, self.data)
        self.assertNotEqual(len(self.errors), 0)

    def test_school_validation_of_incorrect_inputed_address(self):
        """Test incorrect validation of school form
        when address was inputted incorrect.
        """
        self.request.POST['address'] = u'Грушевського'
        school_validate_form(self.request, self.errors, self.data)
        self.assertNotEqual(len(self.errors), 0)

    def test_school_validation_of_incorrect_inputed_director(self):
        """Test incorrect validation of school form
        when director was inputted incorrect.
        """
        self.request.POST['director'] = '999'
        school_validate_form(self.request, self.errors, self.data)
        self.assertNotEqual(len(self.errors), 0)

    def test_school_retuning_the_same_data_after_validation(self):
        """Test of correct returning data dictionary
        after validation.
        """
        school_validate_form(self.request, self.errors, self.data)
        self.assertEqual(self.request.POST['name'], self.data['name'])
        self.assertEqual(self.request.POST['address'], self.data['address'])
        # director field can be empty
        if 'director' in self.request.POST:
            self.assertEqual(self.request.POST['director'],
                             self.data['director_id'])


if __name__ == '__main__':
    unittest.main()
