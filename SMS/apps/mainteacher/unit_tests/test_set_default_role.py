# -*- coding: utf-8 -*-
"""
unit_tests.test_set_default_role

Module contains unittest for set_default_role function from utils
package in mainteacher application.

:copyright: (c) 2015 by Oleksii Omelchuk.
:license: BSD.
"""

import unittest

from ..models.teachers import Teachers
from ..utils.manage_teachers import set_default_role


class TestsMainteacherSetDefaultRole(unittest.TestCase):
    """TestCase class for checking set_default_role function."""

    def setUp(self):
        self.director = Teachers(name='Person for test', login='test',
                                 email='test@gmail.com', role_id=2,
                                 password='test_example')
        self.director.save()

    def tearDown(self):
        self.director.delete()
        del self.director

    def test_set_default_role(self):
        """Testing set teacher role for director."""
        set_default_role(self.director.pk)
        self.director = Teachers.objects.get(id=self.director.pk)
        self.assertEqual(self.director.role_id, 3)


if __name__ == '__main__':
    unittest.main()
