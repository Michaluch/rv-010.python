# -*- coding: utf-8 -*-
"""
unit_tests.test_random_pass

Module contains unittest for random_pass function from utils package in
mainteacher application.

:copyright: (c) 2015 by Oleksii Omelchuk.
:license: BSD.
"""

import unittest

from ..utils.pass_generate import random_pass


class TestsMainteacherPassGenerate(unittest.TestCase):
    """TestCase class for checking random_pass function."""

    def test_pass_generate_length(self):
        """Password should contains 8 symbols."""
        self.assertEqual(len(random_pass()), 8)

    def test_pass_generate_type(self):
        """Password should be string type."""
        self.assertIsInstance(random_pass(), str)


if __name__ == '__main__':
    unittest.main()
