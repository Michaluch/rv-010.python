# -*- coding: utf-8 -*-
"""
unit_tests.test_set_director_role

Module contains unittest for set_director_role function from utils
package in mainteacher application.

:copyright: (c) 2015 by Oleksii Omelchuk.
:license: BSD.
"""

import unittest

from ..models.teachers import Teachers
from ..utils.manage_teachers import set_director_role


class TestsMainteacherSetDirectorRole(unittest.TestCase):
    """TestCase class for checking set_director_role function."""

    def setUp(self):
        self.teacher = Teachers(name='Person2 for test', login='test2',
                                email='test2@gmail.com', role_id=3,
                                password='test_example2')
        self.teacher.save()

    def tearDown(self):
        self.teacher.delete()
        del self.teacher

    def test_set_default_role(self):
        """Testing set director role for teacher."""
        set_director_role(self.teacher.pk)
        self.teacher = Teachers.objects.get(id=self.teacher.pk)
        self.assertEqual(self.teacher.role_id, 2)


if __name__ == '__main__':
    unittest.main()
