# -*- coding: utf-8 -*-
"""
unit_tests.test_mainteacher_validations

Module contains unittest for Validation class from utils package in
mainteacher application.

:copyright: (c) 2015 by Oleksii Omelchuk.
:license: BSD.
"""

import unittest

from ..utils.validation import Validate


class TestsMainteacherValidation(unittest.TestCase):
    """TestCase class for checking Validation class."""

    def setUp(self):
        self.email = 'example@gmail.com'
        self.wrong_email = 'example!2@gmail..com'
        self.login = 'Ivan_Mamay'
        self.wrong_login = '@van_Mamay!'
        self.name = u'Черначук Іван Степанович'
        self.wrong_name = ''
        self.list_school_names = [u'НВК №12', u'НВК-ліцей №19', u'ЗОШ №23',
                                  u'Гімназія "Елітар"']
        self.wrong_school_name = u'школа без імені'
        self.school_address = u'вул. Київська, 5'
        self.wrong_school_address = u'вул макарова'
        self.validate = Validate()

    def tearDown(self):
        del self.email
        del self.wrong_email
        del self.login
        del self.wrong_login
        del self.name
        del self.wrong_name
        del self.list_school_names
        del self.wrong_school_name
        del self.school_address
        del self.wrong_school_address
        del self.validate

    def test_check_email_validation(self):
        """If email is valid - return True, else return - None."""
        self.assertTrue(self.validate.check_email(self.email))
        self.assertIsNone(self.validate.check_email(self.wrong_email))

    def test_check_login_validation(self):
        """If login is valid - return True, else return - None."""
        self.assertTrue(self.validate.check_login(self.login))
        self.assertIsNone(self.validate.check_login(self.wrong_login))

    def test_check_name_validation(self):
        """If name is valid - return True, else return - None."""
        self.assertTrue(self.validate.check_name(self.name))
        self.assertIsNone(self.validate.check_name(self.wrong_name))

    def test_check_school_name_validation(self):
        """If school name is valid - return True,
        else return - None.
        """
        for school_name in self.list_school_names:
            self.assertTrue(self.validate.check_school_name(school_name))
        self.assertIsNone(self.validate.check_school_name(
            self.wrong_school_name))

    def test_check_school_address_validation(self):
        """If school address is valid - return True,
        else return - None.
        """
        self.assertTrue(self.validate.check_school_address(
            self.school_address))
        self.assertIsNone(self.validate.check_school_address(
            self.wrong_school_address))


if __name__ == '__main__':
    unittest.main()
